package ictgradschool.web.lab12.ex3;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Film {
    private int id;
    private String title;

    private List<String> genres;
    private List<String> producers;
    private Map<String, String> actors;

    Film(int id, String title) {
        this.id = id;
        this.title = title;
        this.actors = new HashMap<>();
    }



    int getId() {
        return this.id;
    }

    String getTitle() {
        return this.title;
    }

    List<String> getGenres() {
        return this.genres;
    }

    List<String> getProducers() {
        return this.producers;
    }

    Map<String, String> getActors() {
        return this.actors;
    }

    void addProducer(String s) {
        this.producers.add(s);
    }

    void addActor(String name, String role) {
        this.actors.put(name, role);
    }
}
