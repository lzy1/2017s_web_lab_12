package ictgradschool.web.lab12.ex2;
import ictgradschool.Keyboard;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

public class Exercise02 {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        public static void main(String[]args) throws IOException, SQLException {
            try (ArticleDAO dao = new ArticleDAO(new MySQLDatabase())) {

                while (true) {
                    System.out.printf("input a partial title of an article: ");
                    String s = "%" + Keyboard.readInput() + "%";
                    if (s.equals("%%")) {
                        continue;
                    }
                    //calling method from ArticleDAO class
                    List<Article> articles = dao.allArticles(s);
                    for (Article article : articles) {

                        System.out.println(article.getBody());

                    }
                }


            }

        }
    }
